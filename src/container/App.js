import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import moment from 'moment';
import './App.css';
import * as d3 from 'd3';
import dennyData from '../datasets/denny.csv';

class App extends Component {

  componentWillMount(){
    const dennyDataArr =  [];
    d3.csv(dennyData).then((data)=> {

     const dataArr = data.map((d)=>{
      return dennyDataArr.push({
        startDate: moment(`${d.start_date}`, 'MM/DD/YYYY'),
        startTime: moment(`${d.start_time} ${d.start_ampm}`, 'hh:mm:ss A')
      });
     });

     console.log(dataArr)

     

    
    }).catch((err)=> {
        throw err;
    })
  };

  render() {
    
    return (
        <Container>
        <Row>
          <Col>
            <h3>FullThrottle Labs Assignment</h3>
          </Col>
        </Row>
        </Container>
    );
  }
}

export default App;
